# Presentation de differentes technologies de sauvegarde.




1 - [Préparer une sauvegarde bootable du volume (LV) du système d'un serveur physique.](https://gitlab.com/valentingrebert/guides/-/blob/master/Backups/LVM_snapshot_boom.md)

2 - La sauvegarde incrementielle journalisée et testée.

3 - L'externalisation des sauvegardes.