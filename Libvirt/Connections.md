# Se connecter à un hyperviseur et ses VM via libvirt (virt-manager) , cockpit, ssh et TLS.

---


### Sommaire : 

#### 1. Présentation
#### 2. Cockpit
#### 3. SSH
#### 4. TLS

--- ---

--- ---


### 1. Presentation

Pour gerer nos VM sur un hyperviseur de type kvm/qemu derriere *libvirt* nous devons necessairement securiser le transport entre notre client et notre hyperviseur.

Il existe bien entendu le *secure shell* afin d'acceder à nos VM.

Un systeme orienté serveur est generalement depourvu d'interface grahique or pour certaines raisons parfois nous avons besoin de GUI sur certaines machines. 

Ainsi afin d'acceder à ces machines nous pouvons utiliser le puissant outil *virt-viewer* qui prend en charge les modules internes à qemu.

Il existe une plethore de methodes de transport, entre autres : 

* tls,unix,ssh,ext,tcp,libssh,libssh2

 * Remarque: Nous excluons libssh en raison des énormes problemes de securité de la [CVE-2018-10933](https://www.cvedetails.com/cve/CVE-2018-10933/)


La solution *cockpit* (puissante interface web) sur le :9090 qui permet d'acceder à notre hyperviseur et aux machines virtuelles. (avec le paquet *cockpit-machines* )

Il faut par contre ouvrir le :9090 ce qui dans certains cas n'est pas une option. 

Les deux autres methodes qui vont attirer notre attention sont : *ssh* et *tls*.



--- ---


### 2. Cockpit

Afin d'utiliser cockpit nous gardons à l'esprit que le port :9090 devra être accessible sur notre hyperviseur.

Nous intsallons les outils necessaires :

```bash
root@hyperviseur # dnf install libvirt cockpit cockpit-machines
```

Si ça n'est pas deja fait nous pouvons ajouter un utilisateur au groupe *libvirt* et *cockpit-ws* afin qu'il puisse acceder à l'interface web de cokpit et gerer les VM.

```bash
root@hyperviseur # usermod -a -G libvirt,cockpit-ws user
```

Si aucune regle de pare-feu n'existe et que nous pouvons nous le permettre 

```bash
root@hyperviseur # firewall-cmd --zone=public --add-service=cockpit --permanent
root@hyperviseur # firewall-cmd --reload
```

Lancons notre service *cockpit*: 

```bash
root@hyperviseur # systemctl enable --now cockpit.socket
```

Desormais en accedant à l'interface web de cockpit sur l'hyperviseur nous pourrons gerer nos machines directement depuis le navigateur web de notre client.

`https://IPHYPERVISEUR:9090`


![cockpit__machines](/uploads/a79f0bf5ecec76c0755d002a3c332c92/cockpit__machines.png)




### 3. SSH

La structure de nos commandes de connexion a cette forme :

`driver[+transport]://[username@][hostname][:port]/[path][?extraparameters]`

Afin d'utiliser *virt-manager* avec *ssh* il faudra une commande de type :

```bash
virt-manager -c 'qemu+ssh://hypervisoruser@IPHYPERVISOR:SSHPORT/system?keyfile=/home/user/.ssh/id_HYPERVISORSSHKEY'`
```

Cette commande permet de se connecter graphiquement au *virt-manager* de notre hyperviseur grace à *qemu* et au protocole *ssh* en utilisant notre paire de clés prealablement configurée.

Si nous gerons une multitude de machine il est interessant de créer des aliases: 

Dans `$HOME/.config/libvirt/libvirt.conf` afin que le client de notre host puisse utiliser les alias. Il devra appartenir au groupe *libvirt* autrement les alias iront dans `/etc/libvirt/libvirt.conf`

Exemple: 

```bash
uri_aliases = [
 "hypervisor=qemu+ssh://hypervisoruser@IPHYPERVISOR:SSHPORT/system?keyfile=/home/user/.ssh/id_HYPERVISORSSHKEY'",
 "hypervisor2=qemu+ssh://hypervisoruser@IPHYPERVISOR2:SSHPORT/system?keyfile=/home/user/.ssh/id_HYPERVISOR2SSHKEY'",

]
```

Ces commandes permettent donc à un client de se connecter au *virt-manager* installé sur un hyperviseur.




### 4. TLS



Ressources :

https://libvirt.org/remote.html

https://wiki.libvirt.org/page/TLSSetup

https://wiki.libvirt.org/page/VNCTLSSetup#TLS_should_be_set_up_on_the_servers_first

