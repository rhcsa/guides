# Initiation à systemd


### Preambule :

Systemd est le gestionnaire de service introduit dans differentes distributions GNU/Linux (RHEL, Debian, Arch, ...). Il remplace le lanceur *Upstart* un gestionnaire de script SysVInit.

Il propose des avantages comme des inconvenients mais à été offciellement introduit par defaut dans RHEL 7 Debian 8.

Il existe une controverse assez forte dans le monde de GNU/Linux sur systemd mais il ne s'agit pas de la detailler ici.

Notons qu'il s'agit du sous syteme de GNU/Linux depuis un moment et que des distribution de type RHEL ne sont pas pretes de changer cela.

Ce sous systeme represente une couche vaste et complexe mais propose des outils en cli assez simple à utiliser.

Proposons simplement une vue d'ensemble de cette vaste couche du systeme.



### SOMMAIRE : 

I - Presentation de systemd.

II - Manipulation simple des services.

III - Preparation de services avec systemd.

IV - Utilisation avancée.





#### I - Presentation de systemd.


![Systemd-components](/uploads/7c603f629836f117c37288fff568f57e/Systemd-components.png)





--- ---

___ ___


Ressources :

https://www.linux.com/tutorials/understanding-and-using-systemd/

https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/configuring_basic_system_settings/managing-services-with-systemd_configuring-basic-system-settings