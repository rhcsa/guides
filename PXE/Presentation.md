# Monter un serveur PXE (DHCP TFTP NFS) capable de booter des clients en BIOS/BOOT ou UEFI/SECURE BOOT. 

-----

Le but de ces notes est de comprendre la mise en place du serveur PXE sur une distribution type RHEL/Fedora.

Ce serveur PXE devra être capable de booter differentes distributions GNU/Linux( RHEL Debian) et de prendre en charge les clients
bios/boot et uefi/secure boot.


------

------


Distribution utilisée: **Fedora 30 Server** 


Les protocoles reseau utilisés sont : 

* DHCP (  ISC DHCP SERVER ) dhcp-server.x86_64 **dhcpd**  port *:67 68*
* TFTP ( tftp ) tftp-server.x86_64 **tftp**  port *:69*
* NFS ( nfs v4 ) nfs-utils.x86_64 **nfs-server.service** port *:2049*


Les outils systeme utilisés sont : 

* Syslinux (pxelinux, vesamenu, ... ) 
* Shim (shimx64.efi)
* Grub2 
* Kernels et initrd


Nous limiterons les services afin de ne pas surcharger le serveur et de reduire la masse de configurations, de services et de regles de pare-feu. 

Nous choisissons la version 4 du protocole NFS qui se montre efficace ne necessite pas **rpcbind** et afin de gerer plus facilement les ACL. 

Notez que dans la version 8 de Red Hat le NFS utilise TCP et non plus UDP par defaut.

>  NFS version 4 (NFSv4) works through firewalls and on the Internet, no longer requires an rpcbind service, supports Access Control Lists (ACLs), and utilizes stateful operations. 
>  NFSv4 requires the Transmission Control Protocol (TCP) running over an IP network. 
>  In Red Hat Enterprise Linux 8, NFS over UDP is no longer supported. By default, UDP is disabled in the NFS server. 
------


Ces notes s'inspirent grandement de la __documentation officielle__ mais aussi et surtout de tests dans un lab. 


-------

### Resources :

__Base DHCP + TFTP :__

https://docs.fedoraproject.org/en-US/fedora/f30/install-guide/advanced/Network_based_Installations/

https://fedoramagazine.org/how-to-set-up-a-tftp-server-on-fedora/

__NFS :__

https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/deploying_different_types_of_servers/index

https://doc.fedora-fr.org/wiki/Partage_de_disques_en_r%C3%A9seau_avec_NFS

__Secure Boot :__

https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/7/html/system_administrators_guide/sec-uefi_secure_boot

https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/security_hardening/assembly_securing-rhel-during-installation-security-hardening

------

Enjoy.