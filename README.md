# Guides

### Cet espace à pour But de présenter des guides. Ils peuvent se référer à des installations et des notes explicatives sur des procédures techniques à effectuer sur des serveurs GNU/Linux (type CENTOS/RHEL/FEDORA) .

### Les thèmes abordés sont classés dans des répertoires distincts.

#### 1 - [*Backups*](https://gitlab.com/valentingrebert/guides/-/tree/master/Backups) : Concerne différentes approches et techniques de sauvegardes.

#### 2 - [*FTP*](https://gitlab.com/valentingrebert/guides/-/tree/master/FTP) : Se réfère à la mise en place d'une distribution de fichier par ce protocole.

#### 3 - [*Libvirt*](https://gitlab.com/valentingrebert/guides/-/tree/master/Libvirt) : Se réfère à la mise en place de techniques de virtualisation de type kvm/qemu.

#### 4 - [*Monitoring*](https://gitlab.com/valentingrebert/guides/-/tree/master/Monitoring) : Présente quelques pistes sur les logs, le monitoring, la supervision et l'hypervision.

#### 5 - [*PXE*](https://gitlab.com/valentingrebert/guides/-/tree/master/PXE) : Propose une installation d'un serveur de déploiement PXE moderne.

#### 6 - [*SSH*](https://gitlab.com/valentingrebert/guides/-/tree/master/SSH) : Propose une installation et sécurisation de l'incontournable service Secure Shell.

#### 7 - [*Systemctl*](https://gitlab.com/valentingrebert/guides/-/tree/master/Systemctl) Présente le maniement général et quelques astuces de la technologies systemd.

#### 8 - [*Web*](https://gitlab.com/valentingrebert/guides/-/tree/master/Web) Présente la mise en place de Serveurs Web : Apache, Lighttpd, Nginx.  